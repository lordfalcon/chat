<?php
	class crud{
		private function server(){
			$db=new PDO('mysql:host=127.0.0.1;dbname=chat', 'charlie', '123456');
			return $db;
		}
		public function create($table, $field, $value, $data){
			$db = $this->server();
			$req=$db->prepare('INSERT INTO '.$table.'('.$field.') VALUES('.$value.')');
			//var_dump($req);die();
			$req->execute($data);
		}
		public function readBy($table, $fields,$condition,$data){
			$db = $this->server();
			$req=$db->prepare('SELECT '.$fields.' FROM '.$table.' WHERE '.$condition);
			$req->execute($data);
			return $req;
		}		
		public function read($table, $fields){
			$db = $this->server();
			$req=$db->query('SELECT '.$fields.' FROM '.$table.'');
			return $req;
		}
		public function update($table, $field, $condition, $data){
			$db = $this->server();
			$req=$db->prepare('UPDATE '.$table.' SET '.$field.' WHERE '.$condition.'');
			$req->execute($data);
		}
		public function del($table, $condition){
			$db = $this->server();
			$req=$db->query('DELETE FROM '.$table.' WHERE '.$condition.'');
		}
	}	 
?>
