<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/ui.css">
        <script src="./js/script.js" defer></script>
    </head>

    <body>
        <?php if ($error){echo "<div class='error'>$error</div>";} ?>
        <div class="table">
            <div class="logo">
                <div class="circle"></div>
                <img src="./img/user.svg" alt="">
            </div>
            <form  method="POST" action="index.php?control=login" class="conteneur">
                <div class="row">
                    <input type="text" id="username"  name="username" placeholder="Username"><br/>
                    <p id="usererror"  style="color:red;display:none ">Username inexistant</p>
                    <input type="password" id="password" name="password" placeholder="mot de passe"><br/>
                    <p id="pwderror"  style="color:red;display:none">Mauvais mot de passe</p>
                    <input type="submit"  onmousemove="myMoveFunction()" name="btn" value="Connexion" class="btn">
                    <a  style="color:crimson" href="index.php?control=register">Inscrivez-vous </a>
                </div>    
            </form> 
        </div>    
       
    </body>
</html>